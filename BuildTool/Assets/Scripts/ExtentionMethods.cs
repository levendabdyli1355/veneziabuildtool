using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtentionMethods
{
    public static Vector3 Round(this Vector3 v)
    {
        v.x = Mathf.RoundToInt(v.x);
        v.y = Mathf.RoundToInt(v.y);
        v.z = Mathf.RoundToInt(v.z) ;

        return v;
    }

    public static Vector3 Round(this Vector3 v, float step)
    {
        return (v / step).Round() * step;
    }

    public static Vector3 Round(this Vector3 v, float step, Vector3 unitHeight)
    {
        Vector3 x = new Vector3(0, 0, 0);
        return ((v + unitHeight + x).Round(step)) - unitHeight;
    }

    public static float AtLeast(this float v, float min)
    {
        return Mathf.Max(v, min);
    }
}
