using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;

public enum BuildType { Planes, BuildingFloors, Walls, Windows, Doors, Roofs, None }

public class BuildManagerTool : EditorWindow
{
    [MenuItem("Window/Tools/BuildTool")]
    public static void OpenBuildTool() => GetWindow<BuildManagerTool>();

    //BuildTool Data
    public GameObject[] items;
    public GameObject selectedItem;
    private Quaternion currRotation;
    public BuildType buildType;
    public BuildType buildFloor;

    //BuildGrid Data
    public float gridSize = 1f;
    public float gridExtent = 1f;

    Dictionary<BuildType, BuildType[]> relations = new Dictionary<BuildType, BuildType[]>();

    GameObject selectedFolder;
    [SerializeField] bool[] selectedPrefabs;

    //Properties
    SerializedObject so;
    SerializedProperty _buildType;
    SerializedProperty _gridSize;
    SerializedProperty _gridExtent;

    Dictionary<BuildType, GameObject[]> folderData;

    private void OnEnable()
    {
        so = new SerializedObject(this);

        //BuildEditor
        _buildType = so.FindProperty("buildType");

        //SnapEditor
        _gridSize = so.FindProperty("gridSize");
        _gridExtent = so.FindProperty("gridExtent");
        gridSize = EditorPrefs.GetFloat("BUILDTOOL_gridSize", 1f);
        gridExtent = EditorPrefs.GetFloat("BUILDTOOL_gridExtent", 1f);
        
        currRotation = Quaternion.identity;

        //relations declaration
        relations.Add(BuildType.Planes, new BuildType[] { BuildType.None });
        relations.Add(BuildType.BuildingFloors, new BuildType[] { BuildType.Planes, BuildType.Walls });
        relations.Add(BuildType.Walls, new BuildType[] { BuildType.BuildingFloors, BuildType.Walls });
        relations.Add(BuildType.Windows, new BuildType[] { BuildType.Walls });
        relations.Add(BuildType.Doors, new BuildType[] { BuildType.Windows });
        relations.Add(BuildType.Roofs, new BuildType[] { BuildType.Planes });

        //Data Setup
        folderData = new Dictionary<BuildType, GameObject[]>();

        for (int i = 0; i < (int)BuildType.None; i++)
        {
            string[] guids = AssetDatabase.FindAssets("t:prefab", new[] { "Assets/Prefabs/BuildAssets/" + (BuildType)i });
            IEnumerable<string> paths = guids.Select(AssetDatabase.GUIDToAssetPath);
            GameObject[] copyArray = paths.Select(AssetDatabase.LoadAssetAtPath<GameObject>).ToArray();
            folderData.Add((BuildType)i, copyArray);
        }

        buildType = BuildType.BuildingFloors;
        SetupSelected(buildType);
        selectedItem = folderData[buildType].ElementAt(0);

        SceneView.duringSceneGui += DuringSceneGUI;
    }

    private void OnDisable()
    {
        EditorPrefs.SetFloat("BUILDTOOL_gridSize", gridSize);
        EditorPrefs.SetFloat("BUILDTOOL_gridExtent", gridExtent);

        SceneView.duringSceneGui -= DuringSceneGUI;
    }

    private void OnGUI()
    {
        so.Update();
        //BuildEditor
        EditorGUILayout.PropertyField(_buildType);

        //SnapEditor
        EditorGUILayout.PropertyField(_gridSize);
        EditorGUILayout.PropertyField(_gridExtent);

        if (so.ApplyModifiedProperties())
        {
            SceneView.RepaintAll();
        }

        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            GUI.FocusControl(null);
            Repaint();
        }
    }

    void DuringSceneGUI(SceneView sceneView)
    {

        TogglePrefabs();

        DrawGrid();

        RotateObject();

        Transform camTf = sceneView.camera.transform;

        if (Event.current.type == EventType.MouseMove)
        {
            sceneView.Repaint();
        }

        ChangeBuildTypes();

        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit) && hit.collider != null)
        {
            Handles.DrawAAPolyLine(5, hit.point, hit.point + hit.normal * 0.4f);

            foreach(BuildType type in relations[buildType])
            {
                if (hit.collider.CompareTag(type.ToString()))
                {
                    //if(buildType == BuildType.Windows)
                    //{
                    //    PierceWalls();
                    //}
                    //else
                    //{
                        DrawPrefab(hit, sceneView);

                        if (Event.current.keyCode == KeyCode.Space && Event.current.type == EventType.KeyDown)
                        {
                            TrySpawnObject(hit);
                        }
                    //}
                }
            }
        }
    }

    void PierceWalls()
    {

    }

    void TogglePrefabs()
    {
        Handles.BeginGUI();

        Rect rect = new Rect(8, 8, 64, 64);

        foreach (GameObject item in items)
        {
            Texture icon = AssetPreview.GetAssetPreview(item);

            if (GUI.Toggle(rect, this.selectedItem == item, new GUIContent(icon)))
            {
                this.selectedItem = item;
            }

            rect.y += rect.height + 2;
        }
        Handles.EndGUI();

    }

    void ChangeBuildTypes()
    {
        Event key = Event.current;

        switch(key.keyCode)
        {
            case KeyCode.Keypad1:
                buildType = BuildType.Planes;
                Repaint();
                SetupSelected(buildType);
                break;

            case KeyCode.Keypad2:
                buildType = BuildType.BuildingFloors;
                Repaint();
                SetupSelected(buildType);
                break;

            case KeyCode.Keypad3:
                buildType = BuildType.Walls;
                Repaint();
                SetupSelected(buildType);
                break;

            case KeyCode.Keypad4:
                buildType = BuildType.Windows;
                Repaint();
                SetupSelected(buildType);
                break;

            case KeyCode.Keypad5:
                buildType = BuildType.Doors;
                Repaint();
                SetupSelected(buildType);
                break;

            case KeyCode.Keypad6:
                buildType = BuildType.Roofs;
                buildType = BuildType.Roofs;
                Repaint();
                
                break;
        }
    }

    void SetupSelected(BuildType bt)
    {
        items = folderData[bt];

        if (selectedPrefabs == null || selectedPrefabs.Length != items.Length)
        {
            selectedPrefabs = new bool[items.Length];
        }
    }

    void DrawPrefab(RaycastHit hit, SceneView sceneView)
    {
        Matrix4x4 pointToWorldMtx = Matrix4x4.TRS((hit.point + hit.normal * 0.8f).Round(gridSize, new Vector3(0,0.5f,0)), Quaternion.identity * currRotation, Vector3.one);

        MeshFilter[] filters = selectedItem.GetComponentsInChildren<MeshFilter>();

        foreach (MeshFilter filter in filters)
        {
            Matrix4x4 childToPoint = filter.transform.localToWorldMatrix;
            Matrix4x4 childToWorldMtx = pointToWorldMtx * childToPoint;

            Mesh mesh = filter.sharedMesh;
            Material mat = filter.GetComponent<MeshRenderer>().sharedMaterial;
            mat.SetPass(0);

            Graphics.DrawMesh(mesh, childToWorldMtx, mat, 0, sceneView.camera);
        }
    }

    void TrySpawnObject(RaycastHit hit)
    {
        if (selectedItem == null) return;

        selectedFolder = GameObject.Find(buildType.ToString());

        if (selectedFolder == null)
        {
            selectedFolder = new GameObject(buildType.ToString());
        }

        Vector3 pos = (hit.point + hit.normal * 0.8f).Round(gridSize, new Vector3(0, 0.5f, 0));
        Quaternion rot = Quaternion.identity * currRotation;      

        GameObject spawnedItem = (GameObject)PrefabUtility.InstantiatePrefab(selectedItem);
        spawnedItem.transform.SetParent(selectedFolder.transform);

        spawnedItem.transform.position = pos;
        spawnedItem.transform.rotation = rot;

        if(buildType == BuildType.Walls)
        {
            spawnedItem.GetComponent<Wall>().PlaceWallsAround();
        }

        Undo.RegisterCreatedObjectUndo(spawnedItem, "Item");
    }

    void RotateObject()
    {
        if(Event.current.keyCode == KeyCode.KeypadPlus && Event.current.type == EventType.KeyDown)
        {
            currRotation *= Quaternion.Euler(0, 90, 0);
        }
        if (Event.current.keyCode == KeyCode.KeypadMinus && Event.current.type == EventType.KeyDown)
        {
            currRotation *= Quaternion.Euler(0, -90, 0);
        }
    }

    void DrawGrid()
    {
        if (Event.current.type == EventType.Repaint)
        {

            Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;

            int lineCount = Mathf.RoundToInt((gridExtent * 2) / gridSize);

            if (lineCount % 2 == 0)
            {
                lineCount++;
            }

            int halfLineCount = lineCount / 2;

            for (int i = 0; i < lineCount; i++)
            {
                int offsetIndex = i - halfLineCount;
                float xCoord = offsetIndex * gridSize + 0.5f;
                float zCoord0 = halfLineCount * gridSize + 0.5f;
                float zCoord1 = -halfLineCount * gridSize + 0.51f;

                Vector3 p0 = new Vector3(xCoord, 0f, zCoord0);
                Vector3 p1 = new Vector3(xCoord, 0f, zCoord1);
                Handles.DrawAAPolyLine(p0, p1);

                Vector3 p2 = new Vector3(zCoord0, 0f, xCoord);
                Vector3 p3 = new Vector3(zCoord1, 0f, xCoord);
                Handles.DrawAAPolyLine(p2, p3);
            }
        }
    }


}
