using UnityEngine;
using UnityEditor;


[ExecuteAlways]
public class Wall : MonoBehaviour
{

    public GameObject prefabSelf;

    public void PlaceWallsAround()////
    {
        for (int i = 0; i < 4; i++)
        {
            Vector3 direction = transform.forward + transform.localPosition;
            switch(i)
            {
                case 0: direction = Vector3.forward; break;
                case 1: direction = Vector3.right; break;
                case 2: direction = Vector3.left; break;
                case 3: direction = Vector3.back; break;
                    default: break;
            }
            CheckNearWalls(direction);
        }
    }

    void CheckNearWalls(Vector3 direction)
    {
        
        Debug.DrawRay(gameObject.transform.localPosition, direction * 1f, Color.blue, 5f);

        RaycastHit hit;

        if (Physics.Raycast(transform.localPosition, direction * 1f, out hit,1f))
        {

            GameObject spawnedItem = (GameObject)PrefabUtility.InstantiatePrefab(prefabSelf);
            spawnedItem.transform.position = transform.localPosition + direction * 0.5f;
            spawnedItem.transform.SetParent(gameObject.transform.parent);
            Undo.RegisterCreatedObjectUndo(spawnedItem, "Item");
        }
    }


}
