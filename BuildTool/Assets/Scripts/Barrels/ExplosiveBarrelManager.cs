using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ExplosiveBarrelManager : MonoBehaviour{

    public static List<ExplosiveBarrel> barrelsList = new List<ExplosiveBarrel>();



    #if UNITY_EDITOR
    private void OnDrawGizmos()
    {

        Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;

        foreach (ExplosiveBarrel barrel in barrelsList)
        {
            if (barrel.data == null) continue;
            Vector3 managerPos = transform.position;
            Vector3 barrelPos = barrel.transform.position;
            float halfHeight = (managerPos.y - barrelPos.y) * .5f;
            Vector3 offset = Vector3.up * halfHeight;

            Handles.DrawBezier(
                managerPos,
                barrelPos,
                managerPos - offset,
                barrelPos + offset,
                barrel.data.color,
                EditorGUIUtility.whiteTexture,
                1f
            );

        }
    }
    #endif
}
