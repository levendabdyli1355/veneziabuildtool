using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteAlways]
public class ExplosiveBarrel : MonoBehaviour {

    public BarrelData data;
    MaterialPropertyBlock mpb;

    static readonly int shPropColor = Shader.PropertyToID("_Color");

    MaterialPropertyBlock Mpb {
        get{
            if(mpb == null)
                mpb = new MaterialPropertyBlock();
            return mpb;
        }
    }

    void OnDrawGizmosSelected() {

        if (data == null)
            return;
        Handles.color = data.color;
        Handles.DrawWireDisc(transform.position, transform.up, data.radius);
        Handles.color = Color.white;

    }

    //called on property change
    private void OnValidate() => TryApplyColor();
    
    void OnEnable()
    {
        ExplosiveBarrelManager.barrelsList.Add(this);
    }

    void OnDisable()
    {
        ExplosiveBarrelManager.barrelsList.Remove(this);
    }
    void TryApplyColor()
    {
        if (data == null) 
            return;
        MeshRenderer rnd = GetComponent<MeshRenderer>();
        Mpb.SetColor(shPropColor, data.color);
        rnd.SetPropertyBlock(Mpb);
    }


}
